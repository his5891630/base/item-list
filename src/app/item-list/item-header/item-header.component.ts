import { Component, EventEmitter, Input,Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Item } from '../items';
@Component({
  selector: 'his-item-header',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './item-header.component.html',
  styleUrls: ['./item-header.component.scss']
})
export class ItemHeaderComponent {
@Input() item!:Item;
@Output() delete = new EventEmitter<Item>;
@Output() update = new EventEmitter<Item>;
@Output() insert = new EventEmitter<Item>;


}
