import { Component, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemBodyComponent } from './item-body/item-body.component';
import { ItemHeaderComponent } from './item-header/item-header.component';
import { Item } from './items';

@Component({
  selector: 'app-item-list',
  standalone: true,
  imports: [CommonModule, ItemBodyComponent, ItemHeaderComponent],
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
})
export class ItemListComponent {
  items = signal([]);
  onModifyArticle:any;
  onRemoveArticle:any;
  // #articleService: ArticleService = inject(ArticleService);
  // articles: WritableSignal<Item[]> = signal([]);

  // async ngOnInit() {
  //   try {
  //     this.articles.set(await this.#articleService.getArticles());
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  // async onRemoveArticle(item: Item) {
  //   try {
  //     await this.#articleService.removeArticle(article.id);
  //     this.articles.mutate((x) => {
  //       x.splice(x.findIndex((x) => x.id === article.id),1);
  //     });
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  // async onModifyArticle(item: Item) {
  //   try {
  //     await this.#articleService.modifyArticle(article);
  //     this.articles.mutate((x) => {
  //       x[x.findIndex((x) => x.id == article.id)].title = article.title;
  //     });
  //   } catch (e) {
  //     console.error(e);
  //   }
  // }
}
